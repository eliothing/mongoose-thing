let { Schema } = require("mongoose")

let modelSchema = new Schema(
  {
    potentialAction: { type: "String" },
    identifier: { type: "String" },
    sameAs: { type: "String" },
    url: { type: "String" },
    image: { type: "String" },
    alternateName: { type: "String" },
    name: { type: "String" },
    description: { type: "String" },
    mainEntityOfPage: { type: "String" },
    disambiguatingDescription: { type: "String" },
    subjectOf: { type: "String" },
    additionalType: { type: "String" },
    owner_id: { type: "String" }
  },
  { strict: "throw", toObject: { versionKey: false } }
)
