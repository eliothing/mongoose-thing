"use strict"
const fs = require("fs")
const mongoose = require("mongoose")
const should = require("chai").should()

const MongooseThingBuilder = require("../mongoose-thing-builder")
const schema_path = "schemaorg/data/releases/9.0/schemaorg-all-http.jsonld"
const schema_contents = fs.readFileSync(schema_path, "utf-8")
const SCHEMA = JSON.parse(schema_contents)

const modelDef = {
  fields: {
    potentialAction: { type: "Text" },
    identifier: { type: "Text" },
    sameAs: { type: "URL" },
    url: { type: "URL" },
    image: { type: "URL" },
    alternateName: { type: "Text", index: true },
    name: { type: "Text", required: true, unique: true },
    description: { type: "Text" },
    mainEntityOfPage: { type: "URL" },
    disambiguatingDescription: { type: "Text" },
    subjectOf: { type: "Text" },
    additionalType: { type: "URL" }
  },
  name: "Thing",
  subs: []
}

describe("module | MongooseThingBuilder", function(hooks) {
  it("fetches MongooseThingBuilder", () => {
    let mongooseThingBuilder = new MongooseThingBuilder(
      SCHEMA["@graph"],
      "http://schema.org/"
    )
    let modelSchema = mongooseThingBuilder.mongooseSchema(modelDef)
    let Thing = mongoose.model("Thing", modelSchema)
    Thing.should.be.ok
  })
  it("saves instance of CrispMongoose model", function() {
    var mocks = {
      name: "Thing 1",
      alternateName: "This is really Thing 1",
      disambiguatingDescription: "This disambiguates Thing 1",
      description: "This describes Thing 1",
      nonField: "This is not a valid field."
    }
    let mongooseThingBuilder = new MongooseThingBuilder(
      SCHEMA["@graph"],
      "http://schema.org/"
    )
    let modelSchema = mongooseThingBuilder.mongooseSchema(modelDef)
    let Thing = mongoose.model("Thing", modelSchema)
    let thing = new Thing(mocks)
    thing.save()
    thing.name.should.eql(mocks.name)
    thing.alternateName.should.eql(mocks.alternateName)
    thing.disambiguatingDescription.should.eql(mocks.disambiguatingDescription)
    thing.description.should.eql(mocks.description)
  })
})
