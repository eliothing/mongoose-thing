# Quickstart mongoose-thing

- [mongoose-thing Prerequisites](/eliothing/mongoose-thing/prerequisites.html)
- [Installing mongoose-thing](/eliothing/mongoose-thing/installing.html)

## Nutshell

```
const fs = require("fs")
const mongoose = require("mongoose")
const MongooseThingBuilder = require("@elioway/mongoose-thing-builder")

const schema_path = "./schemaorg/data/releases/9.0/schemaorg-all-http.jsonld"
const schema_contents = fs.readFileSync(schema_path, "utf-8")
const SCHEMA = JSON.parse(schema_contents)


let mongooseThingBuilder = new MongooseThingBuilder(SCHEMA["@graph"], "http://schema.org/")
let thingDef = mongooseThingBuilder.modelMaker("Thing", ["Thing", "Place"], {help: false})
let thingSchema = mongooseThingBuilder.mongooseSchema(thingDef)
let Thing = mongoose.model("Thing", thingSchema)
let thing = new Thing({identifier: "New Thing"})
thing.save()
```
