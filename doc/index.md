<aside>
  <dl>
  <dd>when they come to model Heaven</dd>
  <dd>And calculate the stars, how they will wield</dd>
  <dd>The mighty frame; how build, unbuild, contrive</dd>
  <dd>To save appearances</dd>
</dl>
</aside>

Converts <https://schema.org> into **MongooseJs** Modal objects which you can instantiate and save it to a **MongoDb**.

# Seeing is Believing

```
const fs = require("fs")
const mongoose = require("mongoose")
const MongooseThingBuilder = require("@elioway/mongoose-thing-builder")

const schema_path = "./schemaorg/data/releases/9.0/schemaorg-all-http.jsonld"
const schema_contents = fs.readFileSync(schema_path, "utf-8")
const SCHEMA = JSON.parse(schema_contents)


let mongooseThingBuilder = new MongooseThingBuilder(SCHEMA["@graph"], "http://schema.org/")
let thingDef = mongooseThingBuilder.modelMaker("Thing", ["Thing", "Place"], {help: false})
let thingSchema = mongooseThingBuilder.mongooseSchema(thingDef)
let Thing = mongoose.model("Thing", thingSchema)
let thing = new Thing({identifier: "New Thing"})
thing.save()
```
