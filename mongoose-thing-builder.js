"use strict"
const mongoose = require("mongoose")
const ThingBuilder = require("@elioway/thing")

module.exports = class MongooseThingBuilder extends ThingBuilder {
  /**
   * @file Extends ThingBuilder with a fucntion to return a mongoosejs Schema object
   * @author Tim Bushell
   *
   * @tutorial In this function, which you use as a starting point for future
   * cripsy modules, we take the modefDef returned by ThingBuilder.modelMaker() and
   * convert it into a mongoose.Schema object.
   *
   * @param {Object} modefDef e.g.:
   * >> {
   * >>   fields: {
   * >>      identifier: { type: "Text", unique: true },
   * >>      url: { type: "URL" },
   * >>      name: { type: "Text" },
   * >>      description: { type: "Text" },
   * >>   },
   * >>   name: "Thing",
   * >>   subs: [],
   * >> }
   *
   * @returns {mongoose.Schema}
   */
  mongooseSchema(modefDef, opts) {
    // Map from Schema primitive type names to MongoDb primitive type name.
    if (!opts || !opts.hasOwnProperty("typeMap")) {
      if (!opts) opts = new Object()
      opts["typeMap"] = {
        Boolean: Boolean,
        Date: Date,
        DateTime: Date,
        Number: Number,
        Float: mongoose.Decimal128,
        Integer: Number,
        Text: String,
        URL: String,
        Time: Date,
        Comment: String,
        Distance: Number,
        PropertyValue: String,
        Quantity: Number
      }
    }

    let Thing = undefined
    // Already registered?
    if (mongoose.modelSchemas.hasOwnProperty(modefDef.name)) {
      // Use the ready-registered schema
      return mongoose.modelSchemas[modefDef.name]
    } else {
      let fields = new Object()
      // Build a new mongoose Schema using `modefDef`
      for (let [fieldName, fieldDef] of Object.entries(modefDef.fields)) {
        let field = new Object()
        if (fieldDef.index) field.index = true
        if (fieldDef.required) field.required = true
        if (fieldDef.unique) field.unique = true
        if (fieldDef.foreign) {
          // Type is another SCHEMA: Return as mongoose relationship.
          field.ref = fieldDef.type
          field.type = mongoose.ObjectId
        } else {
          field.type = opts.typeMap[fieldDef.type]
        }
        if (!field.type) throw `${fieldName}: type ${field.type} unknown`
        fields[fieldName] = field
      }
      return new mongoose.Schema(fields)
    }
  }
}
