![](https://elioway.gitlab.io/eliothing/mongoose-thing/elio-mongoose-Thing-logo.png)

> Mongoose, **the elioWay**

# mongoose-thing ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

Converts <https://schema.org> into **MongooseJs** Modal objects of Things which you can instantiate and save to a **MongoDb**.

- [mongoose-thing Documentation](https://elioway.gitlab.io/eliothing/mongoose-thing/)

## Installing

- [Installing mongoose-thing](https://elioway.gitlab.io/eliothing/mongoose-thing/installing.html)

## Requirements

- [eliothing Prerequisites](https://elioway.gitlab.io/eliothing/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [eliothing Quickstart](https://elioway.gitlab.io/eliothing/quickstart.html)
- [mongoose-thing Quickstart](https://elioway.gitlab.io/eliothing/mongoose-thing/quickstart.html)

# Credits

- [mongoose-thing Credits](https://elioway.gitlab.io/eliothing/mongoose-thing/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/eliothing/mongoose-thing/apple-touch-icon.png)
